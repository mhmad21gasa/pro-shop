import styled from 'styled-components';
import { FlexCol,FlexBox } from '../../Global.style';



export const CardBox= styled(FlexCol)`
    width: 100%;
    height: 224px;
`
export const DescreptionBox= styled(FlexBox)`
    width: 100%;
    height: 130px;
    background: #F2F2F2 0% 0% no-repeat padding-box;
    opacity: 1;
`